from django.urls import path
from . import views
from .views import PortalView

urlpatterns = [
    path('', PortalView.as_view(), name='portal'),
]

